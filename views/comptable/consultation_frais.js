jQuery(document).ready(function () {

	var data = []; // Initialisation du tableau de data.
	var btnEditFrais = false;

	//Au changement de la liste utilisateur
	$("#liste_utilisateurs").change(function () {
		if (btnEditFrais == true) {
			var save = confirm("Sauvegarder les modifications ?");//Demande svg s'il quitte EditFrais
			if (save == true) {
				showModifFrais()				//sauvegarde
				$('#btnSaveFrais').hide(400);

			} else {
				return false;  //annuler
			}
		}
		$("#periode option").remove();				//On vide la liste des périodes
		$("#liste_frais tr").remove();				//On vide le tableau
		var dataSend = $("#liste_utilisateurs").serialize();
		btnEditFrais = false;
		dataSend[btnEditFrais] = [btnEditFrais]; //Vérif si EditFrais
		sendPeriode(dataSend) 		//function ajax
	});


	//Au changement de la liste période	
	$("#periode").change(function () {
		$("#loader1").show();
		$("#liste_frais tr").remove();//On vide le tableau
		if (btnEditFrais == true) {
			$('#btnSaveFrais').hide(400);
		}
		var dataSend = $("#liste_utilisateurs").serialize(); //Récup de l'id de l'utilisateur
		btnEditFrais = false;
		dataSend[btnEditFrais] = [btnEditFrais]; //Vérif si EditFrais
		sendPeriode(dataSend); //function ajax
	});


	//Au clik du bouton EditFrais
	$("#btnEditFrais").click(function () {
		if (btnEditFrais == true) {
			var quit = confirm("Voulez vraiment quittez sans sauvegarder ?");
			if (quit == true) {
				btnEditFrais = false;
				$("#liste_frais tr").remove();
				console.log("quitter");
				console.log(btnEditFrais);
				$('#btnSaveFrais').hide(400);
				var dataSend = $("#liste_utilisateurs").serialize();
				dataSend[btnEditFrais] = [btnEditFrais];
				sendPeriode(dataSend);
			} else {
				return false;
			}
		} else {
			$("#liste_frais tr").remove();//On vide le tableau
			var dataSend = $("#liste_utilisateurs").serialize();
			btnEditFrais = true;
			dataSend[btnEditFrais] = [btnEditFrais];
			sendPeriode(dataSend);
			$('#btnSaveFrais').toggle(400);
			//console.log(dataSend);
		}
	});


	//Sauvegarde EditFrais
	$("#btnSaveFrais").click(function () {
		var save = confirm("Sauvegarder les modifications ?");
		if (save == true) {
			showModifFrais()
			btnEditFrais = false;
			$("#liste_frais tr").remove().delay(800);//On vide le tableau
			var dataSend = $("#liste_utilisateurs").serialize();
			$('#btnSaveFrais').toggle(400);
			sendPeriode(dataSend);
		} else {
			return false;
		}
	});



	//AJAX liste periodes
	function sendPeriode(dataSend) {
		$.ajax({
			type: 'POST',      // envoi des données en POST
			url: 'validation_frais2.php',     // envoi au fichier défini dans l'attribut action
			data: {
				dataSend: dataSend
				, periode: $('#periode').val()
			},    // données à envoyer
			success: function (data) {     // callback en cas de succès	    	
				$("#loader1").show();
				var thead = "<tr>"; //tableau frais 
				if (btnEditFrais == true) {//Vérif frais edit
					thead += "<th>Select</th>";
				}
				thead += "<th>N° frais</th><th>Type de frais</th><th>Libellé</th><th>Coûts en €</th><th>Date</th><th>Statut</th><th>Mobile</th></tr>";
				$('#liste_frais').append(thead);
				$.each(data.frais, function (cle, valeur) {	//Boucle	//On récupère les données et on prépare les lignes du tableau
					var frais = [];
					var i = 0;
					var content = "<tr class='data'>";	//Début de ligne            		            	
					$.each(valeur, function (cle2, valeur2) {
						frais[i] = [cle2, valeur2];
						i++;
						//console.log("cle :"+ cle  +", valeur :"+ valeur +", cle2 ="+ cle2 + ", valeur2 ="+ valeur2);
					});
					if (btnEditFrais == true) {//Vérif frais edit
						content += "<td><input type='checkbox' class='chkboxfrais' name='" + frais[0][1] + "' value=''></td>";
					}
					content += "<td class='id_frais'>" + frais[0][1] + "</td>";
					content += "<td>" + frais[1][1] + " </td>";
					if (btnEditFrais == true) {//Vérif frais edit
						var str = frais[2][1];
						var res = str = str.replace(/'/g, "&apos;");
						console.log("res : " + res);
						content += "<td><input class='libelle' name='libelle' value='" + res + "' </input></td>";
					} else {
						content += "<td>" + frais[2][1] + " </td>";
					}
					content += "<td>" + frais[3][1] + " </td>";
					content += "<td>" + frais[4][1] + " </td>";

					if (btnEditFrais == true) {//Vérif frais edit
						content += "<td><select class='edited_statut' name='edited_statut'>";
						content += "<option value='" + frais[5][1] + "'>" + frais[5][1] + "</option>";
						content += "<option value='Remboursé'>Remboursé</option>";
						content += "<option value='Non remboursable'>Non remboursable</option>";
						content += "<option value='En cours de traitement'>En cours de traitement</option>";
						content += "</select></td>";
					} else {
						content += "<td>" + frais[5][1] + " </td>";
					}

					content += "<td>" + frais[6][1] + " </td>";
					content += "</tr>"
					$('#liste_frais').append(content);     //Insertion des lignes dans le tableau
					//console.log(content);
				}); //Fin de boucle
				$('#fraisEdit').find('input[type="checkbox"]').shiftSelectable(); //Function raccourcis clavier
				$("#periode").trigger("chosen:updated");
				$("#loader1").hide(); //Loader
			}
		});
	}

	//AJAX Choix utilisateur
	function sendData(dataSend) {
		console.log(dataSend);
		$.ajax({
			type: 'POST',      // envoi des données en POST
			url: 'validation_frais2.php',     // envoi au fichier défini dans l'attribut action
			data: {
				dataSend: dataSend
				, periode: $('#periode').val()
			},    // données à envoyer
			success: function (data) {     // callback en cas de succès
				var id_user = $('#liste_utilisateurs option:selected').val();
				if (typeof (data["periodes"]) != "undefined" && data["periodes"] !== null) {//On vérifie qu'il y a bien des périodes de retounées		        				
					$.each(data["periodes"], function (k, v) {//On récupère les périodes selon l'utilisateur sélectionné
						$('#periode').append(new Option(v, v));//On ajoute une balise option pour chaque période
					});
					$('#btnEditFrais').show(400);//Bouton EditFrais
				} else {
					$('#periode').append(new Option("Aucune", 0));//Aucune période
					$('#btnEditFrais').hide(400);
				}
				$("#periode").trigger("chosen:updated");
				$("#loader1").hide();
			}
		});
	}

	//editfrais

	function showValues() {
		$("#loader1").show();
		var dataSend = $("#liste_utilisateurs").serialize();//Formulaire
		dataSend[btnEditFrais] = [btnEditFrais];
		sendData(dataSend);
	}

	$("#liste_utilisateurs").on("change", showValues);


	function showModifFrais() {//Récupération données avant sauvegarde
		$("#loader1").show();
		var i = 0;
		var fraisModif = [];
		$('#liste_frais').find(".data").each(function () {
			var id_frais = $(this).find(".id_frais").text();
			var libelle = $(this).find(".libelle").val();
			var edited_statut = $(this).find(".edited_statut").val();
			fraisModif[i] = [id_frais, libelle, edited_statut];
			i++;
		});
		//  console.log(fraisModif);
		saveModif(fraisModif);
	}


	function saveModif(fraisModif) {    //Envoie des données pour sauvegarde
		$.ajax({
			type: 'POST',      // envoi des données en POST
			url: 'saveEdit.php',     // envoi au fichier défini dans l'attribut action
			data: {
				fraisModif: fraisModif
				, periode: $('#periode').val()
			},    // données à envoyer
			success: function (data) {     // callback en cas de succès

			}
		});
	}


	//	$('#fraisEdit').keyup(function(touche){ // on écoute l'évènement keyup()
	//
	//	    var appui = touche.which || touche.keyCode; // le code est compatible tous navigateurs grâce à ces deux propriétés
	//
	//	    if(appui == 16){ // si le code de la touche est égal à 16 (Entrée)
	//	        alert('Vous venez d\'appuyer sur la touche Shift !'); 
	//	    }
	//	});






	//function raccourcis clavier


	var chkboxesfrais = $('.chkboxfrais');


	$.fn.shiftSelectable = function () {
		var lastChecked,
			$chkboxesfrais = this;


		$chkboxesfrais.click(function (evt) {
			if (!lastChecked) {
				lastChecked = this;
				return;
			}

			if (evt.shiftKey) {

				var start = $chkboxesfrais.index(this),
					end = $chkboxesfrais.index(lastChecked);
				$chkboxesfrais.slice(Math.min(start, end), Math.max(start, end) + 1)
					.prop('checked', lastChecked.checked) //prop() instead of .attr()
					.trigger('change');
			}

			lastChecked = this;
		});
	};
});
