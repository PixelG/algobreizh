<?php
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
    if(isset($_SESSION['statut'])==2 )
    {
        ?>

<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="utf-8">
<title>Consultation frais</title>
<meta name="Author" lang="fr" content="GAMARDE Sébastien & SAMSON Denis & PLAISIER Sylvain"> 
<meta name="description" content="Appli Frais Algobreizh" />
<meta name="robots" content="noindex, nofollow, noarchive" />

 <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!--  Font-Awesome Style -->
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--  Animation Style -->
    <!--  Google Font Style -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!--  Custom Style -->
    <link href="../assets/css/style.css" rel="stylesheet" />
</head>

<body>
<?php include '../content/nav.php';?>
     <div class="row pad-top-botm">
             <div class="row text-center ">
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    <h2 data-wow-delay="0.3s" class="wow rollIn animated"><strong>Suivi de mes frais</strong></h2>
                    <p class="sub-head">Consulter vos dernières entrées en toute sérénité.</p>
                    
                </div>
            </div>
        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2 text-center">
                <h4>Période d'engagement : </h4>
                <form name="listePeriodes"  method="post" action="../visiteur/consultation_frais.php">
                    <div class="form-group">
                        <select name="periode" id="periode" onChange="this.form.submit()">
                        <option style="display:none;" value = "choisir">
                            <?php 
                            if (isset($_POST['periode'])) {
                                echo $_POST['periode'];
                            }
                            else echo "Période";
                            ?>
                        </option>
                        <?php
                            while ($donnes = $liste_periode->fetch())
                          {
                            echo'<option value="'.$donnes['periode_frais'].'">'.$donnes['periode_frais'].'</option>';
                          }
                        ?>
                </select>
                    </div>
                </form>
                
          </div>
                    <table class="table table-condensed text-center">
                    <tr><th>Type de frais</th><th>Libellé</th><th>Coûts en €</th><th>Date</th><th>Statut</th></tr>
                    <!--    Afficher les frais de la période correspondante à la liste periode au dessus> -->
                    <?php
                    if (isset($_POST['periode'])) {
                    while ($donnees = $table_frais->fetch()) {
                            echo'<tr><td>'.$donnees['nom'].'</td><td>'.$donnees['libelle'].'</td><td>'.$donnees['montantReel'].'</td><td>'.$donnees['date_frais'].'</td><td>'.$donnees['statut'].'</td></tr>';
                            }
                            echo $donnees['nom'];
                            }
                    ?>
                    </table>
       
    </div>
                <script src="../assets/js/jquery-1.10.2.js"></script>
    <!--  Core Bootstrap Script -->
    <script src="../assets/js/bootstrap.js"></script>
</body>	
</html>
<?php
}
    else
    echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
    window.location.replace("/algobreizh");
                    </script>';
}
else
echo'<script>alert("Veuillez vous identifier !");
    window.location.replace("/algobreizh");
                    </script>';
?>

