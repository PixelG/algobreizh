<?php
session_start();
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
	if(isset($_SESSION['statut'])==2)
	{
		// importation des données d'accès à la base de donnée

		// importation de la méthode d'accès à la base de donnée
		require_once '../models/connexion/connectdb.php';
		// importation des modèles de données à traiter
		require_once '../models/visiteur/functions.php';

		/*On charge la liste d'ENUM de la colonne 'nom' de la table TypesFrais*/
		$liste_periode = liste_periode(3, $_SESSION['id']);
		if (isset($_POST['periode'])) {
		$table_frais = get_frais($_POST['periode'], $_SESSION['id']);
		}
		//On charge la vue en dernier
		require_once '../views/visiteur/consultation_frais.php';
		
	}
	else
		echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
	window.location.replace("/algobreizh");
</script>';
}
else
	echo'<script>alert("Veuillez vous identifier !");
window.location.replace("/algobreizh");
</script>';
?>