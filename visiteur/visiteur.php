<?php
session_start();
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
	if(isset($_SESSION['statut'])==2)
	{
		
		//On charge la vue en dernier
		require_once '../views/visiteur/visiteur.php';
		
	}
	else
		echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
	window.location.replace("/algobreizh");
</script>';
}
else
	echo'<script>alert("Veuillez vous identifier !");
window.location.replace("/algobreizh");
</script>';
?>