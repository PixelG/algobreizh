<?php
/* CONTROLLEUR DE LA PAGE INSCRIPTION des administrateur */
session_start();
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
	if(isset($_SESSION['statut'])==1)
	{
	// importation de la méthode d'accès à la base de donnée
	require_once '../models/connexion/connectdb.php';
	// importation des modèles de données à traiter
	require_once '../models/admin/inscription.php';
		if (isset ( $_POST ['inscription'] ))  // on vérifie que le bouton inscription à bien été cliqué
		{	// on prépare nos regex pour vérifier que l'utilisateur ne rentre pas n'importe quoi
			$pattern_email = "#^(\w[-._+\w]*\w@\w[-._\w]*\w\.\w{2,3})$#";
			$pattern_telephone = "#^((\+|00)33\s?|0)[1-9](\s?\d{2}){4}$#";
			$pattern_general = "#^[0-9-a-zA-Zàâäéèêëïîôöùûüç ]+$#";
				if (! empty ( $_POST ['username'] ) && ! empty ( $_POST ['nom'] ) && 
					! empty ( $_POST ['prenom'] ) && 
					! empty ( $_POST ['adresse1'] ) && 
					! empty ( $_POST ['codePostal'] ) && 
					! empty ( $_POST ['ville'] ) && 
					! empty ( $_POST ['pays'] ) && 
					! empty ( $_POST ['telephonePortable'] ) && 
					! empty ( $_POST ['email'] ) && 
					(preg_match ( $pattern_general, $_POST ['username'] )) 
					&& (preg_match ( $pattern_general, $_POST ['nom'] )) 
					&& (preg_match ( $pattern_general, $_POST ['prenom'] )) 
					&& (preg_match ( $pattern_general, $_POST ['pays'] )) 
					&& (preg_match ( $pattern_general, $_POST ['adresse1'] )) 
					&& (preg_match ( $pattern_general, $_POST ['codePostal'] )) 
					&& (preg_match ( $pattern_general, $_POST ['ville'] )) 
					&& (preg_match ( $pattern_telephone, $_POST ['telephonePortable'] )) 
					&& (preg_match ( $pattern_email, $_POST ['email'] ))) 
				{
					// on initialise les variables reçi du formulaire d'inscription
					$username = htmlspecialchars ( $_POST ['username'] );
					$nom = htmlspecialchars ( $_POST ['nom'] );
					$prenom = htmlspecialchars ( $_POST ['prenom'] );
					$adresse1 = htmlspecialchars ( $_POST ['adresse1'] );
					$adresse2 = htmlspecialchars ( $_POST ['adresse2'] );
					$pays = htmlspecialchars ( $_POST ['pays'] );
					$codePostal = htmlspecialchars ( $_POST ['codePostal'] );
					$ville = htmlspecialchars ( $_POST ['ville'] );
					$telephoneFixe = htmlspecialchars ( $_POST ['telephoneFixe'] );
					$telephonePortable = htmlspecialchars ( $_POST ['telephonePortable'] );
					$email = htmlspecialchars ( $_POST ['email'] );
					$password = password_hash(random_password ( 8 ),PASSWORD_BCRYPT, array("cost" => 10));
					$statut = ($_POST ['statut']);
					$civilite = ($_POST['civilite']);
					
					if (($_POST ['statut']) == "administrateur") {
						$statut = 1;
					} 
					elseif (($_POST ['statut']) == "commercial") {
						$statut = 2;
					} 
					elseif (($_POST ['statut']) == "comptable") {
						$statut = 3;
					}
					else {
						$statut = 4;
					}
				}
			if (isset ( $_POST ['username'] ) && (! preg_match ( $pattern_general, $_POST ['username'] ))) {
				echo '
										<script> alert("Vous devez renseigner un username valide !");  
			         					window.location.replace("../admin/inscription.php");
										</script>';
			}
			if (isset ( $_POST ['nom'] ) && (! preg_match ( $pattern_general, $_POST ['nom'] ))) {
				
				echo '
										<script> alert("Vous devez renseigner un nom valide !");  
			         					window.location.replace("../admin/inscription.php");
										</script>';
			}
			if (isset ( $_POST ['prenom'] ) && (! preg_match ( $pattern_general, $_POST ['prenom'] ))) {
				echo '
										<script> alert("Vous devez renseigner un prénom valide !");  
			         					window.location.replace("../admin/inscription.php");
										</script>';
			}
			if (isset ( $_POST ['pays'] ) && (! preg_match ( $pattern_general, $_POST ['pays'] ))) {
				echo '
										<script> alert("Vous devez renseigner un pays valide !");  
			         					window.location.replace("../admin/inscription.php");
										</script>';
			}
			if (isset ( $_POST ['adresse1'] ) && (! preg_match ( $pattern_general, $_POST ['adresse1'] ))) {
				echo '
										<script> alert("Vous devez renseigner une adresse valide !");  
			         					window.location.replace("../admin/inscription.php");
										</script>';
			}
			if (!empty ( $_POST ['adresse2'] ) && (! preg_match ( $pattern_general, $_POST ['adresse2'] ))) {
				echo '
										<script> alert("Vous devez renseigner une adresse2 valide !");  
			         					window.location.replace("../admin/inscription.php");
										</script>';
			}
			
			if (isset ( $_POST ['codePostal'] ) && (! preg_match ( $pattern_general, $_POST ['codePostal'] ))) {
				echo '
											<script> alert("Vous devez renseigner un code postal valide !");  
				         					window.location.replace("../admin/inscription.php");
											</script>';
			}
			
			if (isset ( $_POST ['ville'] ) && (! preg_match ( $pattern_general, $_POST ['ville'] ))) {
				echo '
										<script> alert("Vous devez renseigner une ville valide !");  
			         					window.location.replace("../admin/inscription.php");
										</script>';
			}
			if (!empty( $_POST ['telephoneFixe']) && isset ( $_POST ['telephoneFixe'] ) && (! preg_match ( $pattern_telephone, $_POST ['telephoneFixe'] ))) {
				echo '
										<script> alert("Vous devez renseigner un numéro de téléphone valide !");  
			         					window.location.replace("../admin/inscription.php");
										</script>';
			}

			if (isset ( $_POST ['telephonePortable'] ) && (! preg_match ( $pattern_telephone, $_POST ['telephonePortable'] ))) {
				echo '
										<script> alert("Vous devez renseigner un numéro de téléphone valide !");  
			         					window.location.replace("../admin/inscription.php");
										</script>';
			}
			if (isset ( $_POST ['email'] ) && (! preg_match ( $pattern_email, $_POST ['email'] ))) {
				echo '
										<script> alert("Vous devez renseigner un Email valide !");  
			         					window.location.replace("../admin/inscription.php");
										</script>';
			}
			
			/* Vérification de la disponibilité du username dans la bdd */
			$count = username_est_dispo ( $prenom, $nom, $username, $telephonePortable, $email );
			if ($count == 1) {
				// username déjà utilisé
				echo "<script>
						            alert('L'utilisateur que vous essayez d'inscrire existe déjà !')
						    		</script>";
			} 
			else {
				/* Insertion des information de la personne dans la bdd */
				
				insertUtilisateur ( $username, $password, $statut, $civilite, $nom, $prenom, $adresse1, $adresse2, $codePostal, $ville, $pays, $telephoneFixe, $telephonePortable, $email );
				echo "<script>
							            alert('Le membre à bien été ajouté !')
							    		</script>";
				/* envoi d'un mail à l'interessé avec son username et son password */
				send_pass ( $email, $username, $password );
			}
		// importation des données d'accès à la base de donnée
		
}
	require_once '../views/admin/inscription.php';
	}
	else
	echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
	window.location.replace("/algobreizh");
					</script>';
}
else
echo'<script>alert("Veuillez vous identifier !");
	window.location.replace("/algobreizh");
					</script>';
?>