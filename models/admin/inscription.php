<?php
 /** Cette requete ne prend aucun paramètre en entrée elle se contente d'entrer
  *un Utilisateur dans la bdd
  */
 function insertUtilisateur($username,
                            $password,
                            $statut,
                            $civilite,
                            $nom,
                            $prenom,
                            $adresse1,
                            $adresse2,
                            $codePostal,
                            $ville,
                            $pays,
                            $telephoneFixe,
                            $telephonePortable,
                            $email)
 {
 $bdd=connectdb(); // connection à la base de donnée
  $insertUtilisateurs = " INSERT INTO Utilisateurs(
                            username,
                            password,
                            statut,
                            civilite,
                            nom,
                            prenom,
                            adresse1,
                            adresse2,
                            codePostal,
                            ville,
                            pays,
                            telephoneFixe,
                            telephonePortable,
                            email)
      VALUES( 
                          '$username',
                          '$password',
                          '$statut',
                          '$civilite',
                          '$nom',
                          '$prenom',
                          '$adresse1',
                          '$adresse2',
                          '$codePostal',
                          '$ville',
                          '$pays',
                          '$telephoneFixe',
                          '$telephonePortable',
                          '$email'
                  
                  )
            ";
      $req = $bdd->prepare($insertUtilisateurs);
      $req->execute(array(
        'username' => $username,
        'nom' => $nom,
        'prenom' =>$prenom,
        'civilite'=>$civilite,
        'statut' =>$statut,
        'password' => $password,
        'adresse1' =>$adresse1,
        'adresse2' =>$adresse2,
        'codePostal' =>$codePostal,
        'ville' => $ville,
        'telephoneFixe' => $telephoneFixe,
        'telephonePortable' => $telephonePortable,
        'pays' => $pays,
        'email' => $email));
      $req-> fetch();
}
/** Cette fonction vérifie lors de l'inscription de l'utilisateur
* si l'utilisateur est déjà dans la base de donnée ou non.
* if($count == 1) username déjà utilisé
* else il faut créer le compte.
*/
function username_est_dispo($prenom, $nom, $username, $telephonePortable, $email){
    $bdd=connectdb(); // connection à la base de donnée
    $query = $bdd->query("SELECT idUtilisateurs FROM Utilisateurs
                                  WHERE prenom = '$prenom'
                                  AND nom = '$nom'
                                  AND username = '$username'
                                  AND telephonePortable = '$telephonePortable'
                                  AND email = '$email'");
			$count = $query->rowCount();
		return $count;
}
  /** Cette fonction prend en paramètre le nombre de caractères de la chaine que vous souhaitez
*générer et les caractères que vous utiliserez pour la composer
*On se sert alors du résultat comme mot de passe généré automatiquement lors de
*l'inscription d'un nouveau membre.
*/
  function random_password($nb_car, $chaine = 'azertyuiopqsdfghjklmwxcvbn123456789')
{
    $nb_lettres = strlen($chaine) - 1;
    $password = '';
    for($i=0; $i < $nb_car; $i++)
    {
        $pos = mt_rand(0, $nb_lettres);
        $car = $chaine[$pos];
        $password .= $car;
    }
    return $password;
}
  /** Cette fonction prend en paramètre une variable dans laquelle est stockée une adresse mail
*et envoie un mail contenant le username et le password à l'interessé lors de son inscription.
*/
	function send_pass($email,$username,$password) {
     $to      = $email;
     $subject = 'Votre mot de passe Algobreizh';
     $message = 'Bonjour ! Félicitation votre demande de compte sur l\'interface Algobreizh a été traité !\n
     			 Votre identifiant est : '.$username.'
     			 \nVotre mot de passe est : '.$password.'';
     $headers = 'From: webmaster@algobreizh.com' . "\r\n" .
     /*'Reply-To: webmaster@algobreizh.com' . "\r\n" .*/
     'X-Mailer: PHP/' . phpversion();

     mail($to, $subject, $message, $headers);
 }
?>
