<?php
require_once '../models/connexion/connectdb.php';

function get_list_utilisateurs()
{
  $bdd = connectdb();
  $req = $bdd->query("SELECT idUtilisateurs , nom , prenom FROM Utilisateurs WHERE statut = 2");
  $resultat = $req;
  return $resultat;
}

function liste_periodes($id_utilisateur)
{
  $bdd = connectdb();
  $req = $bdd->query("SELECT DISTINCT periode_frais FROM Frais WHERE id_utilisateur = $id_utilisateur");
  $resultat = $req->fetchAll();
  return $resultat;
}


function get_frais_comptable($periode_frais, $idUtilisateurs)
{
  $bdd = connectdb(); // connection à la base de donnée
  $query = "SELECT DISTINCT f.idFrais, f.id_utilisateur, Utilisateurs.nom, Utilisateurs.prenom, Utilisateurs.telephonePortable, f.libelle, f.montantReel, f.date_frais, f.statut, f.id_utilisateur 
  FROM Frais f 
  JOIN Utilisateurs 
  ON f.id_utilisateur = Utilisateurs.idUtilisateurs
  AND f.id_utilisateur = " . $idUtilisateurs . "
  AND f.periode_frais = '" . $periode_frais . "'
  ORDER BY f.idFrais;";
  $req = $bdd->query($query);
  $resultat = $req;  //->fetchAll();

  return $resultat;
}

function editfrais($id_frais, $libelle, $statut)
{
  $bdd = connectdb(); // connection à la base de donnée
  $edit = "UPDATE Frais SET libelle = :libelle, statut = :statut WHERE idFrais = :id_frais";
  $req = $bdd->prepare($edit);
  $req->execute(array(
    'id_frais' => $id_frais,
    'libelle' => $libelle,
    'statut' => $statut
  ));
  $req->fetch();
}
