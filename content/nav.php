<!-- MENU NAV DES PAGES -->


<div class="navbar navbar-default navbar-fixed-top move-me ">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                    <img src="../assets/img/logo-algobreizh.png" class="navbar-brand-logo" alt="" />

            </div>
            <div class="navbar-collapse collapse move-me">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a data-toggle="dropdown" href="#"><?php echo ''.$_SESSION['username'].' ';?><i class="fa fa-navicon "></i></a>
                        <ul class="dropdown-menu">
                        	<?php
                        	
                            if (($_SESSION['statut']) == 3 ){ // c'est un comptable
                            	echo' <li><a href="../comptable/validation_frais.php">Valider des frais</a></li>';
                        	}
                        	if (($_SESSION['statut']) == 1){ // c'est un admin
								echo' <li><a href="../admin/inscription.php">Ajout utilisateur</a></li>
                            	<li><a href="../admin/suppression.php">Suppression utilisateur</a></li>
                                <li><a href="../admin/ajout_statut.php">Ajout statut</a></li>';
							}
                        	if (($_SESSION['statut']) == 2 || ($_SESSION['statut']) == 5){ // c'est un visiteur
                            echo' <li><a href="../visiteur/visiteur.php">Gestion de mes frais</a></li>
                            <li><a href="../visiteur/gestion_frais.php">Saisir des frais</a></li>
                            <li><a href="../visiteur/consultation_frais.php">Consulter mes frais</a></li>';
                       		}
                        	?>     
                        	<!-- c'est tout le monde -->
                            <li><a href="../session/profil.php">Profil</a></li>
                            <li><a href="../session/deconnexion.php">Fermer la session</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

        </div>
    </div>