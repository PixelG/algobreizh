-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 03 Avril 2015 à 15:45
-- Version du serveur: 5.6.14
-- Version de PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `algobreizh`
--

-- --------------------------------------------------------

--
-- Structure de la table `Frais`
--

CREATE TABLE IF NOT EXISTS `Frais` (
  `idFrais` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `id_typeFrais` tinyint(4) NOT NULL,
  `id_utilisateur` smallint(4) unsigned NOT NULL,
  `libelle` varchar(128) NOT NULL,
  `montantReel` float unsigned NOT NULL,
  `date_frais` date NOT NULL,
  `periode_frais` date NOT NULL,
  `statut` varchar(64) NOT NULL,
  PRIMARY KEY (`idFrais`),
  KEY `id_utilisateur_idx` (`id_utilisateur`),
  KEY `id_typeFrais` (`id_typeFrais`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `Frais`
--

INSERT INTO `Frais` (`idFrais`, `id_typeFrais`, `id_utilisateur`, `libelle`, `montantReel`, `date_frais`, `periode_frais`, `statut`) VALUES
(1, 0, 34, 'Test', 354, '2015-02-23', '2012-01-01', 'En cours de traitement'),
(3, 1, 34, 'Test du deuxieme frais', 19, '0000-00-00', '2013-01-01', 'En cours de traitement'),
(4, 3, 34, 'Ceci est un libellé', 33, '2015-02-23', '2015-02-01', 'Non remboursable'),
(5, 3, 34, 'Nuit chez ma soeur', 20, '2015-03-15', '2015-03-01', 'En cours de traitement'),
(6, 3, 34, 'Nuitée au formule 1', 60, '2015-03-15', '2015-03-01', 'En cours de traitement'),
(7, 0, 34, 'Aller-retour Paris-Rennes en voiture', 80, '2015-02-27', '2015-03-01', 'En cours de traitement'),
(10, 0, 34, 'Exploit', 45, '2015-02-10', '2015-03-01', 'En cours de traitement'),
(11, 0, 34, 'Exploit', 45, '2015-02-10', '2015-03-01', 'En cours de traitement'),
(12, 5, 34, 'Planète e-commerce Vannes', 28, '2015-03-25', '2015-03-01', 'En cours de traitement'),
(13, 1, 34, 'Aller-retour Paris-Rennes en voiture', 56, '2015-03-13', '2015-03-01', 'En cours de traitement'),
(14, 1, 41, 'Test', 40, '2015-03-18', '2015-03-01', 'Remboursé');

-- --------------------------------------------------------

--
-- Structure de la table `Justificatifs`
--

CREATE TABLE IF NOT EXISTS `Justificatifs` (
  `idJustificatifs` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_frais` int(7) unsigned NOT NULL,
  `nom` varchar(16) NOT NULL,
  `chemin` varchar(255) NOT NULL,
  PRIMARY KEY (`idJustificatifs`),
  UNIQUE KEY `chemin_UNIQUE` (`chemin`),
  KEY `id_frais_idx` (`id_frais`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Statuts`
--

CREATE TABLE IF NOT EXISTS `Statuts` (
  `idStatuts` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `statut` varchar(16) NOT NULL,
  PRIMARY KEY (`idStatuts`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `Statuts`
--

INSERT INTO `Statuts` (`idStatuts`, `statut`) VALUES
(1, 'Administrateur'),
(2, 'Commercial'),
(3, 'Comptable'),
(4, 'Direction'),
(14, 'Nouveau statut');

-- --------------------------------------------------------

--
-- Structure de la table `TypesFrais`
--

CREATE TABLE IF NOT EXISTS `TypesFrais` (
  `id_typeFrais` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(16) NOT NULL,
  PRIMARY KEY (`id_typeFrais`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `TypesFrais`
--

INSERT INTO `TypesFrais` (`id_typeFrais`, `nom`) VALUES
(1, 'Déplacement'),
(2, 'Restauration'),
(3, 'Hébergement'),
(4, 'Evènementiel'),
(5, 'Conférence'),
(6, 'Hors Forfait');

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateurs`
--

CREATE TABLE IF NOT EXISTS `Utilisateurs` (
  `idUtilisateurs` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(128) NOT NULL,
  `statut` tinyint(1) unsigned NOT NULL,
  `civilite` enum('Monsieur','Madame') NOT NULL,
  `nom` varchar(32) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `adresse1` varchar(255) NOT NULL,
  `adresse2` varchar(255) DEFAULT NULL,
  `codePostal` varchar(16) NOT NULL,
  `ville` varchar(64) NOT NULL,
  `pays` varchar(64) NOT NULL,
  `telephoneFixe` int(10) unsigned zerofill DEFAULT NULL,
  `telephonePortable` int(10) unsigned zerofill NOT NULL,
  `email` varchar(128) NOT NULL,
  `dateInscription` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idUtilisateurs`),
  UNIQUE KEY `login_UNIQUE` (`username`),
  UNIQUE KEY `telephonePortable_UNIQUE` (`telephonePortable`),
  UNIQUE KEY `mul_Unique` (`nom`,`prenom`,`adresse1`,`ville`,`pays`),
  KEY `fk_statut_idx` (`statut`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 PACK_KEYS=1 AUTO_INCREMENT=42 ;

--
-- Contenu de la table `Utilisateurs`
--

INSERT INTO `Utilisateurs` (`idUtilisateurs`, `username`, `password`, `statut`, `civilite`, `nom`, `prenom`, `adresse1`, `adresse2`, `codePostal`, `ville`, `pays`, `telephoneFixe`, `telephonePortable`, `email`, `dateInscription`) VALUES
(31, 'Admin', '$2y$10$jHuYPtj.ij7YGG/5eq8Ea.F610V.xxfDbFqN8TFH31Lu5x7nGedKK', 1, 'Monsieur', 'Gamarde', 'Sébastien', '10 rue Kerbautet', 'Non renseigné', '35150', 'Corps-Nuds', 'France', 0656782455, 0669417592, 'sebastien.gamarde@mailoo.org', '2014-11-06 22:51:13'),
(33, 'Comptable', '$2y$10$jHuYPtj.ij7YGG/5eq8Ea.F610V.xxfDbFqN8TFH31Lu5x7nGedKK', 3, 'Monsieur', 'Comptable', 'Comptable', '2 rue des compeurs', 'Non renseigné', '35150', 'Comptable', 'France', 0243027728, 0600000000, 'comptable@comptable.fr', '2014-12-12 10:05:19'),
(34, 'Commercial', '$2y$10$jHuYPtj.ij7YGG/5eq8Ea.F610V.xxfDbFqN8TFH31Lu5x7nGedKK', 2, 'Monsieur', 'Commercial', 'Commercial', 'Rue du commerce', '', '35000', 'Rennes', 'France', 0000000000, 0607080910, 'sebastien.gamarde@commercial.fr', '2014-12-19 13:52:14'),
(41, 'Direction', '$2y$10$jHuYPtj.ij7YGG/5eq8Ea.F610V.xxfDbFqN8TFH31Lu5x7nGedKK', 4, 'Monsieur', 'Benzina', 'Mathis', '23 Rue Louis Kerautret Botmel', '', '35200', 'Rennes', 'France', 0000000000, 0601020304, 'mathis.benzina@aftec.fr', '2015-03-18 14:06:11');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Frais`
--
ALTER TABLE `Frais`
  ADD CONSTRAINT `id_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `Utilisateurs` (`idUtilisateurs`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Justificatifs`
--
ALTER TABLE `Justificatifs`
  ADD CONSTRAINT `id_frais` FOREIGN KEY (`id_frais`) REFERENCES `gsb_BDD`.`Frais` (`idFrais`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Utilisateurs`
--
ALTER TABLE `Utilisateurs`
  ADD CONSTRAINT `fk_statut` FOREIGN KEY (`statut`) REFERENCES `Statuts` (`idStatuts`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
